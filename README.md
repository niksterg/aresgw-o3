# AresGW-O3

## New Gravitational Wave Discoveries Enabled by Machine Learning

### A. E. Koloniari, E. C. Koursoumpa, P. Nousi, P. Lampropoulos, N.Passalis, A. Tefas, N. Stergioulas 

https://arxiv.org/abs/2407.07820

Abstract:

The detection of gravitational waves has revolutionized our understanding of the universe, offering unprecedented insights into its dynamics. A major goal of gravitational wave data analysis is to speed up the detection and parameter estimation process using machine learning techniques, in light of an anticipated surge in detected events that would render traditional methods impractical. Here, we present the first detections of new gravitational-wave candidate events in data from a network of interferometric detectors enabled by machine learning. We discuss several new enhancements of our ResNet-based deep learning code, AresGW, that increased its sensitivity, including a new hierarchical classification of triggers, based on different noise and frequency filters. The enhancements resulted in a significant reduction in the false alarm rate, allowing AresGW to surpass traditional pipelines in the number of detected events in its effective training range (single source masses between 7 and 50 solar masses and source chirp masses between 10 and 40 solar masses), when the new detections are included. We calculate the astrophysical significance of events detected with AresGW using a logarithmic ranking statistic and injections into O3 data. Furthermore, we present spectrograms, parameter estimation, and reconstruction in the time domain for our new candidate events and discuss the distribution of their properties. In addition, the AresGW code exhibited very good performance when tested across various two-detector setups and on observational data from the O1 and O2 observing periods. Our findings underscore the remarkable potential of AresGW as a fast and sensitive detection algorithm for gravitational-wave astronomy, paving the way for a larger number of future discoveries.

![Spectrogram](Spectrograms.png)
